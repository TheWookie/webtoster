/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.wookie.webtoster.dao.TaskDao;
import com.wookie.webtoster.dao.jdbc.JdbcTaskDao;
import com.wookie.webtoster.entities.Task;
import com.wookie.webtoster.entities.TestTasks;
import com.wookie.webtoster.entities.builder.TaskBuilder;
import com.wookie.webtoster.entities.builder.TestTasksBuilder;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Set;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;

/**
 *
 * @author wookie
 */
public class JdbcTaskDaoTest {
    private class DaoTest extends JdbcTaskDao {
        @Override
        protected Connection getConnection() throws SQLException {
            return TestJdbcDaoFactory.getConnection(); 
        }
    }
    
    private TestJdbcDaoFactory factory = new TestJdbcDaoFactory() {
        @Override
        public TaskDao createTaskDao() {
            return new DaoTest(); //To change body of generated methods, choose Tools | Templates.
        }
    };
    
    private DaoTest dao = (DaoTest)factory.createTaskDao();
    
//    @Test
//    public void testCreate() {
//        Task instance = dao.create(new TaskBuilder()
//                .setText("qeusetion")
//                .setToughness(1)
//                .build());
//        
//        assertNotNull(instance);
//    }

//    @Test
//    public void testDelete() {
//        tutorDao.delete(25);
//    }

    @Test
    public void testFindById() {
        Task instance = dao.findById(1);
        assertNotNull("Find by id", instance);
    }

    @Test
    public void testFindAll() {
        Set<Task> instance = dao.findAll();
        assertEquals("Find all test", false, instance.isEmpty());
    }

//    @Test
//    public void testUpdate() {
//        tutorDao.update(new TutorBuilder()
//                .setId(1)
//                .setName("asd")
//                .setSurname("asdf")
//                .setLogin("asf")
//                .setPassword("asd")
//                .build());
//    }
}
