/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.wookie.webtoster.dao.RightsDao;
import com.wookie.webtoster.dao.jdbc.JdbcRightsDao;
import com.wookie.webtoster.entities.Rights;
import com.wookie.webtoster.entities.StudentTests;
import com.wookie.webtoster.entities.builder.RightsBuilder;
import com.wookie.webtoster.entities.builder.StudentTestsBuilder;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Set;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;

/**
 *
 * @author wookie
 */
public class JdbcRightsDaoTest {
    private class DaoTest extends JdbcRightsDao {
        @Override
        protected Connection getConnection() throws SQLException {
            return TestJdbcDaoFactory.getConnection(); 
        }
    }
    
    private TestJdbcDaoFactory factory = new TestJdbcDaoFactory() {
        @Override
        public RightsDao createRightsDao() {
            return new DaoTest(); //To change body of generated methods, choose Tools | Templates.
        }
    };
    
    private DaoTest dao = (DaoTest)factory.createRightsDao();
    
//    @Test
//    public void testCreate() {
//        Rights instance = dao.create(new RightsBuilder()
//                .setName("right")
//                .build());
//        
//        assertNotNull(instance);
//    }

//    @Test
//    public void testDelete() {
//        tutorDao.delete(25);
//    }

    @Test
    public void testFindById() {
        Rights instance = dao.findById(1);
        assertNotNull("Find by id", instance);
    }

    @Test
    public void testFindAll() {
        Set<Rights> instance = dao.findAll();
        assertEquals("Find all test", false, instance.isEmpty());
    }

//    @Test
//    public void testUpdate() {
//        tutorDao.update(new TutorBuilder()
//                .setId(1)
//                .setName("asd")
//                .setSurname("asdf")
//                .setLogin("asf")
//                .setPassword("asd")
//                .build());
//    }
}
