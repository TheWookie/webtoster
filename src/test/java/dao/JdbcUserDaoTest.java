/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.wookie.webtoster.dao.DaoFactory;
import com.wookie.webtoster.dao.UserDao;
import com.wookie.webtoster.dao.jdbc.JdbcDaoFactory;
import com.wookie.webtoster.dao.jdbc.JdbcUserDao;
import com.wookie.webtoster.entities.User;
import com.wookie.webtoster.entities.builder.RightsBuilder;
import com.wookie.webtoster.entities.builder.UserBuilder;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 *
 * @author wookie
 */
public class JdbcUserDaoTest {
    private class TestUserDao extends JdbcUserDao {
        @Override
        protected Connection getConnection() throws SQLException {
            return TestJdbcDaoFactory.getConnection(); 
        }
        
    } 
    
    private TestJdbcDaoFactory factory = new TestJdbcDaoFactory() {
        @Override
        public TestUserDao createUserDao() {
            return new TestUserDao(); //To change body of generated methods, choose Tools | Templates.
        }
        
    };
    
    private TestUserDao userDao = (TestUserDao)factory.createUserDao();

//    @Test
//    public void testCreate() {
//        User newUser = userDao.create(new UserBuilder()
//                .setName("a")
//                .setSurname("b")
//                .setLogin("c")
//                .setPassword("d")
//                .setRights(new RightsBuilder().setId(2).build())
//                .build());
//        
//        assertNotNull(newUser);
//    }

//    @Test
//    public void testDelete() {
//        tutorDao.delete(25);
//    }

    @Test
    public void testFindById() {
        User user = userDao.findById(1);
        assertNotNull("Find by id", user);
    }

    @Test
    public void testFindAll() {
        Set<User> users = userDao.findAll();
        assertEquals("Find all test", false, users.isEmpty());
    }

//    @Test
//    public void testUpdate() {
//        tutorDao.update(new TutorBuilder()
//                .setId(1)
//                .setName("asd")
//                .setSurname("asdf")
//                .setLogin("asf")
//                .setPassword("asd")
//                .build());
//    }
    
}
