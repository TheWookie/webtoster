/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.wookie.webtoster.dao.TaskAnswersDao;
import com.wookie.webtoster.dao.jdbc.JdbcTaskAnswersDao;
import com.wookie.webtoster.entities.Task;
import com.wookie.webtoster.entities.TaskAnswers;
import com.wookie.webtoster.entities.builder.TaskAnswersBuilder;
import com.wookie.webtoster.entities.builder.TaskBuilder;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Set;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;

/**
 *
 * @author wookie
 */
public class JdbcTaskAnswersDaoTest {
    private class DaoTest extends JdbcTaskAnswersDao {
        @Override
        protected Connection getConnection() throws SQLException {
            return TestJdbcDaoFactory.getConnection(); 
        }
    }
    
    private TestJdbcDaoFactory factory = new TestJdbcDaoFactory() {
        @Override
        public TaskAnswersDao createTaskAnswersDao() {
            return new DaoTest(); //To change body of generated methods, choose Tools | Templates.
        }
    };
    
    private DaoTest dao = (DaoTest)factory.createTaskAnswersDao();
    
//    @Test
//    public void testCreate() {
//        TaskAnswers instance = dao.create(new TaskAnswersBuilder()
//                .setAnswerId(1)
//                .setTaskId(1)
//                .setCorrectness(true)
//                .build());
//        
//        assertNotNull(instance);
//    }

//    @Test
//    public void testDelete() {
//        tutorDao.delete(25);
//    }

    @Test
    public void testFindById() {
        TaskAnswers instance = dao.findById(1);
        assertNotNull("Find by id", instance);
    }

    @Test
    public void testFindAll() {
        Set<TaskAnswers> instance = dao.findAll();
        assertEquals("Find all test", false, instance.isEmpty());
    }

//    @Test
//    public void testUpdate() {
//        tutorDao.update(new TutorBuilder()
//                .setId(1)
//                .setName("asd")
//                .setSurname("asdf")
//                .setLogin("asf")
//                .setPassword("asd")
//                .build());
//    }
}
