/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.wookie.webtoster.dao.TestTasksDao;
import com.wookie.webtoster.dao.jdbc.JdbcTestTasksDao;
import com.wookie.webtoster.entities.TestTasks;
import com.wookie.webtoster.entities.User;
import com.wookie.webtoster.entities.builder.RightsBuilder;
import com.wookie.webtoster.entities.builder.TestTasksBuilder;
import com.wookie.webtoster.entities.builder.UserBuilder;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Set;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;

/**
 *
 * @author wookie
 */
public class JdbcTestTasksDaoTest {
    private class TestTasksDaoTest extends JdbcTestTasksDao {
        @Override
        protected Connection getConnection() throws SQLException {
            return TestJdbcDaoFactory.getConnection(); 
        }
    }
    
    private TestJdbcDaoFactory factory = new TestJdbcDaoFactory() {
        @Override
        public TestTasksDao createTestTasksDao() {
            return new TestTasksDaoTest(); //To change body of generated methods, choose Tools | Templates.
        }
    };
    
    private TestTasksDaoTest dao = (TestTasksDaoTest)factory.createTestTasksDao();
    
//    @Test
//    public void testCreate() {
//        TestTasks instance = dao.create(new TestTasksBuilder()
//                .setTaskId(1)
//                .setTestId(1)
//                .build());
//        
//        assertNotNull(instance);
//    }

//    @Test
//    public void testDelete() {
//        tutorDao.delete(25);
//    }

    @Test
    public void testFindById() {
        TestTasks instance = dao.findById(1);
        assertNotNull("Find by id", instance);
    }

    @Test
    public void testFindAll() {
        Set<TestTasks> instance = dao.findAll();
        assertEquals("Find all test", false, instance.isEmpty());
    }

//    @Test
//    public void testUpdate() {
//        tutorDao.update(new TutorBuilder()
//                .setId(1)
//                .setName("asd")
//                .setSurname("asdf")
//                .setLogin("asf")
//                .setPassword("asd")
//                .build());
//    }
}
