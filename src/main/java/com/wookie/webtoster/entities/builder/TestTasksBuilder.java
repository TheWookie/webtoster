package com.wookie.webtoster.entities.builder;

import com.wookie.webtoster.entities.TestTasks;


public class TestTasksBuilder {
    private TestTasks instance;

    public TestTasksBuilder() {
        instance = new TestTasks();
    }

    public TestTasksBuilder setTaskId(int taskId) {
        instance.setTaskId(taskId);
        return this;
    }

    public TestTasksBuilder setTestId(int testId) {
        instance.setTestId(testId);
        return this;
    }

    public TestTasks build() {
        return instance;
    }
}
