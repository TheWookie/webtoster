package com.wookie.webtoster.entities.builder;

import com.wookie.webtoster.entities.Subject;


public class SubjectBuilder {
    private Subject instance;

    public SubjectBuilder() {
        instance = new Subject();
    }

    public SubjectBuilder setId(int id) {
        instance.setId(id);
        return this;
    }

    public SubjectBuilder setName(String name) {
        instance.setName(name);
        return this;
    }

    public Subject build() {
        return instance;
    }
}
