/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wookie.webtoster.services;

import com.wookie.webtoster.dao.AnswerDao;
import com.wookie.webtoster.dao.DaoFactory;
import com.wookie.webtoster.dao.TaskAnswersDao;
import com.wookie.webtoster.entities.Answer;
import com.wookie.webtoster.entities.TaskAnswers;
import com.wookie.webtoster.entities.builder.TaskAnswersBuilder;
import java.util.logging.Logger;


public class AnswerService {
    private final static Logger logger = Logger.getLogger(TaskService.class.getName());
    private TaskAnswersDao taskAnswersDao = DaoFactory.getFactory().createTaskAnswersDao();
    private AnswerDao answerDao = DaoFactory.getFactory().createAnswerDao();
    
    private static AnswerService instance = new AnswerService();
    
    public static AnswerService getInstance() {
        return instance;
    }
    
    /**
     * Add an answer in database if such answer haven't already exist.
     * @param answer Answer instance.
     * @throws RuntimeException if some trouble with database appears. 
     */
    private Answer createAnswer(Answer answer) throws RuntimeException {
        Answer temp = answerDao.getByText(answer.getText());
        if(temp == null) {
            temp = answerDao.create(answer);
        }
        
        return temp;
    }
    
    /**
     * Add an answer and answer's connection  with task in database if such answer haven't already exist.
     * @param answer Answer instance.
     * @param taskId ID of answer's task.
     * @param correctness true if current answer is correct. False - if not.
     * @throws RuntimeException 
     */
    public void addAnswer(Answer answer, int taskId, boolean correctness) throws RuntimeException {
        Answer temp = createAnswer(answer);
        try { 
            taskAnswersDao.create(new TaskAnswersBuilder()
                        .setAnswerId(temp.getId())
                        .setTaskId(taskId)
                        .setCorrectness(correctness)
                        .build());
        }
        catch(RuntimeException e) {
            logger.warning("Error while processing database " + e);
        }
    }
}
