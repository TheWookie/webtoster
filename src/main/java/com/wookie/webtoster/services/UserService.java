/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wookie.webtoster.services;

import com.wookie.webtoster.controllers.constants.Constants;
import com.wookie.webtoster.controllers.filters.AuthorizationFilter;
import com.wookie.webtoster.dao.DaoFactory;
import com.wookie.webtoster.dao.RightsDao;
import com.wookie.webtoster.dao.UserDao;
import com.wookie.webtoster.entities.StudentTests;
import com.wookie.webtoster.entities.Test;
import com.wookie.webtoster.entities.User;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;

public class UserService {
    private final static Logger logger = Logger.getLogger(UserService.class.getName());
    private static UserService instance = new UserService();

    public static UserService getInstance() {
        return instance;
    }
    
    /**
     * Method finds user by his login.
     * @param login user's login.
     * @return User instance.
     * @throws RuntimeException 
     */
    public User getUser(String login) throws RuntimeException {
        UserDao userDao = DaoFactory.getFactory().createUserDao();
        RightsDao rightsDao = DaoFactory.getFactory().createRightsDao();
        User user = userDao.getByLogin(login);
        user.getRights().setName(rightsDao.findById(user.getRights().getId()).getName());

        return user;
    }

    /**
     * Method checks user's password
     * @param user User instance.
     * @param password inputed password.
     * @throws Exception if password is wrong.
     */
    public void checkPassword(User user, String password) throws Exception {
        if (!user.getPassword().equals(password)) {
            throw new Exception();
        }
    }

    /**
     * Method checks user's rights and decides in which page current user should
     * be redirected.
     *
     * @param request Http request.
     * @param user instance of User class.
     * @return name of page in which user should be redirected.
     * @throws RuntimeException if DAO classes throws exception.
     * @throws Exception if login is wrong.
     */
    public String processUser(HttpServletRequest request, User user) throws RuntimeException {
        logger.info("Starting process user");
        
        TestService testService = TestService.getInstance();
        SubjectService subjectService = SubjectService.getInstance();
        UserDao userDao = DaoFactory.getFactory().createUserDao();

        Set subjects = subjectService.getSubjects();

        switch (user.getRights().getName()) {
            case Constants.TUTOR_RIGHTS:
                Map tests = testService.fillTests(testService.getByUser(user.getId()));
                request.setAttribute(Constants.PROPERTY_RESULT_LIST, tests);
                return Constants.TUTOR_PAGE;
            case Constants.STUDENT_RIGHTS:
                Set tutors = userDao.findByRights(Constants.TUTOR_RIGHTS);
                request.setAttribute(Constants.PROPERTY_TUTOR_LIST, tutors);
                request.setAttribute(Constants.PROPERTY_SUBJECT_LIST, subjects);
                return Constants.STUDENT_PAGE;
            case Constants.ADMIN_RIGHTS:
                request.setAttribute(Constants.PROPERTY_SUBJECT_LIST, subjects);
                return Constants.ADMIN_PAGE;
            default:
                logger.warning("Error while loggin in");
                throw new RuntimeException();
        }

    }
    
}
