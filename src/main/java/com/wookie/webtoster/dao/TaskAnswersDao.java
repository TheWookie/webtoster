package com.wookie.webtoster.dao;

import com.wookie.webtoster.entities.TaskAnswers;
import java.util.Set;


public interface TaskAnswersDao extends GenericDao<TaskAnswers> {
    Set<TaskAnswers> findByTaskId(int taskId);
    Set<TaskAnswers> findByAnswerId(int answerId);
    boolean getCorrectness(int taskId, int answerId);
    //public boolean deleteEntries(int taskId, int answerId);
}
