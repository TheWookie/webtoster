package com.wookie.webtoster.dao.jdbc;

import com.wookie.webtoster.dao.TaskDao;
import com.wookie.webtoster.entities.Subject;
import com.wookie.webtoster.entities.Task;
import com.wookie.webtoster.entities.builder.SubjectBuilder;
import com.wookie.webtoster.entities.builder.TaskBuilder;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;


public class JdbcTaskDao extends JdbcGenericDao implements TaskDao {
    private final static Logger logger = Logger.getLogger(JdbcTaskDao.class.getName());
    public static final String CREATE_STATEMENT =
            "INSERT INTO Task (text, toughness) VALUES (?, ?);";
    public static final String UPDATE_STATEMENT =
            "UPDATE Task SET text=?, toughness=? WHERE id=?;";
    public static final String DELETE_ANSWERS_ENTRIES_STATEMENT =
            "DELETE FROM TaskAnswers WHERE taskId = ?;";
    public static final String DELETE_TESTS_ENTRIES_STATEMENT =
            "DELETE FROM TestTasks WHERE taskId = ?;";
    public static final String DELETE_STATEMENT =
            "DELETE FROM Task WHERE id = ?;";
    public static final String SEARCH_ID =
            "SELECT * from Task WHERE id = ?;";
    public static final String FIND_ALL_STATEMENT =
            "SELECT * from Task;";
    public static final String FIND_BY_TEXT_STATEMENT =
            "SELECT * from Task WHERE text=?;";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_TEXT = "text";
    public static final String COLUMN_TOUGHNESS = "toughness";
    
    private Task getResult(ResultSet rs) throws SQLException {
        return new TaskBuilder()
                        .setId(rs.getInt(COLUMN_ID))
                        .setText(rs.getString(COLUMN_TEXT))
                        .setToughness(rs.getInt(COLUMN_TOUGHNESS))
                        .build();
    }
    
    @Override
    public Task create(Task task) {
        try (Connection cn = getConnection()) {
            PreparedStatement preparedStatement = cn.prepareStatement(CREATE_STATEMENT,  Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, task.getText());
            preparedStatement.setInt(2, task.getToughness());
            preparedStatement.executeUpdate();
            
            ResultSet generatedKeys = preparedStatement.getGeneratedKeys(); 
            if (generatedKeys.next()) 
                task.setId(generatedKeys.getInt(1));
            
            preparedStatement.close();
            
            return task;
        } catch (SQLException e) {
            logger.warning("Error while processing database " + logger.getName());
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean update(Task task) {
        try (Connection cn = getConnection()) {
            PreparedStatement preparedStatement = cn.prepareStatement(UPDATE_STATEMENT);
            preparedStatement.setString(1, task.getText());
            preparedStatement.setInt(2, task.getToughness());
            preparedStatement.setInt(3, task.getId());
            preparedStatement.executeUpdate();
            preparedStatement.close();
            return true;
        } catch (SQLException e) {
            logger.warning("Error while processing database " + logger.getName());
            return false;
        }
    }

    @Override
    public boolean delete(int id) {
        try (Connection cn = getConnection()) {
            PreparedStatement preparedStatement = cn.prepareStatement(DELETE_ANSWERS_ENTRIES_STATEMENT);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
//            preparedStatement = cn.prepareStatement(DELETE_TESTS_ENTRIES_STATEMENT);
//            preparedStatement.setInt(1, id);
//            preparedStatement.executeUpdate();
            preparedStatement = cn.prepareStatement(DELETE_STATEMENT);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
            preparedStatement.close();
            return true;
        } catch (SQLException e) {
            logger.warning("Error while processing database " + logger.getName());
            return false;
        }
    }

    @Override
    public Task findById(int id) {
        try (Connection cn = getConnection()) {
            PreparedStatement preparedStatement = cn.prepareStatement(SEARCH_ID);
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();

            Task temp = null;
            if(rs.next()) {
                temp = getResult(rs);
            }
            preparedStatement.close();
            return temp;
        } catch (SQLException e) {
            logger.warning("Error while processing database " + logger.getName());
            throw new RuntimeException(e);
        }
    }

    @Override
    public Set<Task> findAll() {
        try (Connection cn = getConnection()) {
            Statement query = cn.createStatement();
            ResultSet rs = query.executeQuery(FIND_ALL_STATEMENT);
            Set<Task> res = new HashSet<>();
            while (rs.next()) {
                res.add(getResult(rs));
            }
            query.close();
            return res;
        } catch (SQLException e) {
            logger.warning("Error while processing database " + logger.getName());
            throw new RuntimeException(e);
        }
    }
    
    public Task getByText(String text) {
        try (Connection cn = getConnection()) {
            PreparedStatement preparedStatement = cn.prepareStatement(FIND_BY_TEXT_STATEMENT);
            preparedStatement.setString(1, text);
            ResultSet rs = preparedStatement.executeQuery();

            Task temp = null;
            if(rs.next()) {
                temp = getResult(rs);
            }
            preparedStatement.close();
            return temp;
        } catch (SQLException e) {
            logger.warning("Error while processing database " + logger.getName());
            throw new RuntimeException(e);
        }
    }
}
