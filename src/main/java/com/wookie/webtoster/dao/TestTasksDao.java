package com.wookie.webtoster.dao;

import com.wookie.webtoster.entities.TaskAnswers;
import com.wookie.webtoster.entities.TestTasks;
import java.util.Set;


public interface TestTasksDao extends GenericDao<TestTasks> {
     public Set<TestTasks> findByTestId(int testId);
     public Set<TestTasks> findByTaskId(int taskId);
     public boolean deleteEntries(int testId, int taskId);
}
