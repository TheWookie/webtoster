package com.wookie.webtoster.dao;

import com.wookie.webtoster.entities.Answer;


public interface AnswerDao extends GenericDao<Answer> {
    Answer getByText(String text); 
}
