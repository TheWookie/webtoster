package com.wookie.webtoster.dao;

import com.wookie.webtoster.entities.Task;



public interface TaskDao extends GenericDao<Task> {
    Task getByText(String text); 
}
