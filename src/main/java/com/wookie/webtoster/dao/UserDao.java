package com.wookie.webtoster.dao;

import com.wookie.webtoster.entities.User;
import java.util.Set;


public interface UserDao extends GenericDao<User> {
    User getByLogin(String login);
    
    /**
     * Method finds all Users by rights.
     * @param rights name of right.
     * @return set of users.
     */
    Set<User> findByRights(String rights);
}
